from django.apps import apps
from django.conf.urls import url
from django.contrib.auth.views import (
    LogoutView, PasswordChangeView, PasswordChangeDoneView, PasswordResetView, PasswordResetDoneView,
    PasswordResetConfirmView, PasswordResetCompleteView
)
from django.core.urlresolvers import reverse_lazy

from .views import (
    UserCreateView, UserCreateDoneTemplateView, UserLoginView, UserActivateView
)

urlpatterns = [
    url(r'^login/$',
        UserLoginView.as_view(), name='login'),
    url(r'^logout/$',
        LogoutView.as_view(), name='logout'),
    url(r'^password_change/$',
        PasswordChangeView.as_view(success_url=reverse_lazy('account:password_change_done')), name='password_change'),
    url(r'^password_change/done/$',
        PasswordChangeDoneView.as_view(), name='password_change_done'),
    url(r'^password_reset/$',
        PasswordResetView.as_view(success_url=reverse_lazy('account:password_reset_done')), name='password_reset'),
    url(r'^password_reset/done/$',
        PasswordResetDoneView.as_view(), name='password_reset_done'),

    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirmView.as_view(success_url=reverse_lazy('account:password_reset_complete')),
        name='password_reset_confirm'),
    url(r'^reset/done/$',
        PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    url(r'^register/$',
        UserCreateView.as_view(), name='register'),
    url(r'^register/done/$',
        UserCreateDoneTemplateView.as_view(), name='register-done'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        UserActivateView.as_view(), name='activate')
]

if apps.is_installed('board'):
    from board.views import MessageChangeListView

    urlpatterns += [
        url(r'^messages/$',
            MessageChangeListView.as_view(), name='account-message-change'),
    ]
