import logging

from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.auth.views import LoginView
from django.shortcuts import resolve_url
from django.urls import (
    reverse_lazy
)
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView

from account.forms import (
    WebUserCreationForm, UserLoginForm
)
from .models import User


class UserCreateView(CreateView):
    logger = logging.getLogger(__name__)
    template_name = 'registration/register.html'
    form_class = WebUserCreationForm
    success_url = reverse_lazy('account:register-done')

    def get_context_data(self, **kwargs):
        self.logger.debug('UserCreateView.get_context_data()')
        context = super(UserCreateView, self).get_context_data(**kwargs)
        context['google_recaptcha_site_key'] = settings.GOOGLE_RECAPTCHA_SITE_KEY
        return context

    def get_form_kwargs(self):
        self.logger.debug('UserCreateView.get_form_kwargs()')
        # overrides to pass 'self.request' object
        kwargs = super(UserCreateView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs


class UserCreateDoneTemplateView(TemplateView):
    logger = logging.getLogger(__name__)
    template_name = 'registration/register_done.html'

    def get_context_data(self, **kwargs):
        self.logger.debug('UserCreateDoneTemplateView.get_context_data()')
        context = super(UserCreateDoneTemplateView, self).get_context_data(**kwargs)
        context['email_account_activation'] = settings.EMAIL_ACCOUNT_ACTIVATION
        return context


class UserLoginView(LoginView):
    logger = logging.getLogger(__name__)
    authentication_form = UserLoginForm

    def get_context_data(self, **kwargs):
        self.logger.debug('UserLoginView.get_context_data()')
        context = super(UserLoginView, self).get_context_data(**kwargs)
        context['google_recaptcha_site_key'] = settings.GOOGLE_RECAPTCHA_SITE_KEY
        return context

    def get_success_url(self):
        self.logger.debug('UserLoginView.get_success_url()')
        redirect_to = super(UserLoginView, self).get_success_url()

        # Don't redirect to the following pages after login
        # Don't make users confused!
        if redirect_to in [
            reverse_lazy('account:login'),
            reverse_lazy('account:logout'),
            reverse_lazy('account:register'),
            reverse_lazy('account:register-done'),
            reverse_lazy('account:password_reset'),
            reverse_lazy('account:password_reset_done'),
        ]:
            redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

        return redirect_to


class UserActivateView(TemplateView):
    logger = logging.getLogger(__name__)
    template_name = 'registration/user_activate_complete.html'

    def get(self, request, *args, **kwargs):
        self.logger.debug('UserActivateView.get()')

        uid = force_text(urlsafe_base64_decode(self.kwargs['uidb64']))
        token = self.kwargs['token']

        self.logger.debug('uid: %s, token: %s' % (uid, token))

        try:
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            self.logger.warning('User %s not found' % uid)
            user = None

        if user is not None and PasswordResetTokenGenerator().check_token(user, token):
            user.is_active = True
            user.save()
            self.logger.info('User %s(pk=%s) has been activated.' % (user, user.pk))

        return super(UserActivateView, self).get(request, *args, **kwargs)
