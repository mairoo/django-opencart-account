import json
import logging
import urllib

from django import forms
from django.conf import settings
from django.contrib.auth.forms import (
    ReadOnlyPasswordHashField, AuthenticationForm
)
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _

from .models import (
    User, UserManager
)


class UserLoginForm(AuthenticationForm):
    logger = logging.getLogger(__name__)

    username = forms.EmailField(
        label=_('Email'),
        required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Email address'),
                'required': 'True',
            }
        )
    )
    password = forms.CharField(
        label=_('Password'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Password'),
                'required': 'True',
            }
        )
    )

    def clean(self):
        self.logger.debug('UserLoginForm.clean()')
        # Google reCAPTCHA
        ''' Begin reCAPTCHA validation '''
        recaptcha_response = self.request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req = urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())
        ''' End reCAPTCHA validation '''

        if not result['success']:
            raise forms.ValidationError(_('reCAPTCHA error occurred.'))

        return super(UserLoginForm, self).clean()


class UserCreationForm(forms.ModelForm):
    logger = logging.getLogger(__name__)

    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    email = forms.EmailField(
        label=_('Email'),
        required=True,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Email address'),
                'required': 'True',
            }
        )
    )
    last_name = forms.CharField(
        label=_('Last name'),
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Last name'),
                'required': 'True',
            }
        )
    )
    first_name = forms.CharField(
        label=_('First name'),
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('First name'),
                'required': 'True',
            }
        )
    )
    nickname = forms.CharField(
        label=_('Nickname'),
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Nickname'),
                'required': 'True',
            }
        )
    )
    password1 = forms.CharField(
        label=_('Password'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Password'),
                'required': 'True',
            }
        )
    )
    password2 = forms.CharField(
        label=_('Password confirmation'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Password confirmation'),
                'required': 'True',
            }
        )
    )

    class Meta:
        model = User
        fields = ('email', 'last_name', 'first_name', 'nickname')

    def save(self, commit=True):
        self.logger.debug('UserCreationForm.save()')

        user = super(UserCreationForm, self).save(commit=False)

        user.email = UserManager.normalize_email(self.cleaned_data['email'])
        # Save the provided password in hashed format
        user.set_password(self.cleaned_data['password1'])
        user.is_active = True

        if commit:
            user.save()

        return user

    def clean_password2(self):
        self.logger.debug('UserCreationForm.clean_password2()')

        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_("Passwords don't match"))

        return password2


class UserChangeForm(forms.ModelForm):
    logger = logging.getLogger(__name__)

    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField(
        label=_('Password')
    )

    class Meta:
        model = User
        fields = ('email', 'password', 'last_name', 'first_name', 'is_active', 'is_superuser')

    def clean_password(self):
        self.logger.debug('UserChangeForm.clean_password()')

        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class WebUserCreationForm(UserCreationForm):
    logger = logging.getLogger(__name__)

    terms = forms.BooleanField(
        label=_('Terms of service'),
        required=True,
        initial=False,
        widget=forms.CheckboxInput(
            attrs={
                'required': 'True',
            }
        ),
        error_messages={
            'required': _('You must agree to the Terms of service to sign up'),
        }
    )
    privacy = forms.BooleanField(
        label=_('Privacy policy'),
        required=True,
        initial=False,
        widget=forms.CheckboxInput(
            attrs={
                'required': 'True',
            }
        ),
        error_messages={
            'required': _('You must agree to the Privacy policy to sign up'),
        }
    )

    def __init__(self, *args, **kwargs):
        self.logger.debug('WebUserCreationForm.__init__()')
        # important to "pop" added kwarg before call to parent's constructor
        self.request = kwargs.pop('request')
        super(UserCreationForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        self.logger.debug('WebUserCreationForm.save()')

        user = super(WebUserCreationForm, self).save(commit=False)

        if commit:
            if settings.EMAIL_ACCOUNT_ACTIVATION:
                user.is_active = False
                user.save()

                # Send user activation mail
                current_site = get_current_site(self.request)
                subject = (_('Welcome To %s! Confirm Your Email') % current_site.name)
                message = render_to_string('registration/user_activate_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': PasswordResetTokenGenerator().make_token(user),
                })
                email = EmailMessage(subject, message, to=[user.email])
                email.send()
            else:
                user.is_active = True
                user.save()

        return user

    def clean(self):
        self.logger.debug('WebUserCreationForm.clean()')
        # Google reCAPTCHA
        ''' Begin reCAPTCHA validation '''
        recaptcha_response = self.request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req = urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())
        ''' End reCAPTCHA validation '''

        if not result['success']:
            raise forms.ValidationError(_('reCAPTCHA error occurred.'))

        return super(WebUserCreationForm, self).clean()
