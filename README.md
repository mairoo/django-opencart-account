오픈카트 2.x 호환 계정 관리

# 백엔드 클래스 정의 및 등록

account/backends.py 파일

로그인 인증 처리를 위해 오픈카트 백엔드 클래스의 authenticate() 메소드를 오버라이딩한다.

conf/settings/base.py 파일

```
AUTHENTICATION_BACKENDS = ('account.backends.OpencartBackend',)
```

정의한 오픈카트 백엔드를 사용하기 위해 위와 같이 등록한다.

# 커스텀 사용자 모델 정의 및 등록

account/models.py 파일

Opencart는 사용자별 비밀번호 salt 값을 위해 데이터베이스 필드를 추가한다.

```
salt = models.CharField(verbose_name=_('Salt'), max_length=10, blank=True)
```

회원 가입할 때를 비롯하여 비밀번호를 저장하기 위해 set_password() 메소드를 오버라이딩하고 비밀번호를 변경할 때 비밀번호 확인을 위해 check_password() 메소드를 오버라이딩한다.

conf/settings/base.py 파일

```
AUTH_USER_MODEL = 'account.User'
```

정의한 커스텀 사용자 모델을 Django 프레임워크에서 사용할 수 있도록 설정한다.
